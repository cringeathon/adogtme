from django.db import models
from users.models import Adopter, Shelter


class Offer(models.Model):
    '''
    Creates an offer for a Shelter user.
    '''
    owner = models.ForeignKey(Shelter, on_delete=models.CASCADE, null=True)

    date_published = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=15,verbose_name="Dog's name",null=True)
    age = models.PositiveIntegerField(null=True)
    breed = models.CharField(max_length=20,null=True)
    SEX_CHOICE = [
        ('MALE', 'male'),
        ('FEMALE', 'female'),

    ]
    sex = models.CharField(max_length=7, choices=SEX_CHOICE,null=True)
    weight = models.PositiveIntegerField(null=True)
    sheltered_date = models.DateTimeField(verbose_name='Sheltered on ',null=True)
    found = models.CharField(max_length=30, verbose_name='Found',null=True)

    TRUE_FALSE_CHOICE = [
        (True,'Yes'),
        (False,'No'),
    ]

    is_apartment_friendly = models.BooleanField(verbose_name='Adapts well to apartment living', null=True, choices = TRUE_FALSE_CHOICE)
    is_family_friendly = models.BooleanField(verbose_name='Family friendly', null=True, choices = TRUE_FALSE_CHOICE)
    is_kid_friendly = models.BooleanField(verbose_name='Kid friendly', null=True, choices = TRUE_FALSE_CHOICE)
    is_dog_friendly = models.BooleanField(verbose_name='Dog friendly', null=True, choices = TRUE_FALSE_CHOICE)

    ENERGY_LEVELS = [
        ('LOW', 'Low'),
        ('MEDIUM', 'Medium'),
        ('HIGH', 'High'),
    ]
    energy_level = models.CharField(verbose_name='Energy level', choices=ENERGY_LEVELS, null=True, max_length=10)
    is_good_for_beginners = models.BooleanField(verbose_name='Good for beginners', null=True, choices = TRUE_FALSE_CHOICE)
    desc = models.TextField(max_length=200,verbose_name='Description',null=True)

    def __str__(self):
        return f"offer/{self.owner} on {self.date_published}"

class Preference(models.Model):
    owner = models.OneToOneField(Adopter,on_delete=models.CASCADE)

    age = models.PositiveIntegerField(verbose_name='Animal age')
    breed = models.CharField(max_length=20)
    SEX_CHOICE = [
        ('MALE', 'male'),
        ('FEMALE', 'female'),

    ]
    sex = models.CharField(max_length=7, choices=SEX_CHOICE)
    weight = models.PositiveIntegerField()
    found = models.CharField(max_length=30, verbose_name='Found')

    is_apartment_friendly = models.BooleanField(verbose_name='Adapts well to apartment living', null=True)
    is_family_friendly = models.BooleanField(verbose_name='Family friendly', null=True)
    is_kid_friendly = models.BooleanField(verbose_name='Kid friendly', null=True)
    is_dog_friendly = models.BooleanField(verbose_name='Dog friendly', null=True)

    ENERGY_LEVELS = [
        ('LOW', 'Low'),
        ('MEDIUM', 'Medium'),
        ('HIGH', 'High'),
    ]
    energy_level = models.CharField(verbose_name='Energy level', choices=ENERGY_LEVELS, null=True, max_length=10)
    is_good_for_beginners = models.BooleanField(verbose_name='Good for beginners', null=True)

    def __str__(self):
        return f"pref/{self.owner.adopter_name}"






