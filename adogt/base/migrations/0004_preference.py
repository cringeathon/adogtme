# Generated by Django 3.1.1 on 2020-09-19 20:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20200919_2131'),
        ('base', '0003_auto_20200919_1842'),
    ]

    operations = [
        migrations.CreateModel(
            name='Preference',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('age', models.PositiveIntegerField()),
                ('breed', models.CharField(max_length=20)),
                ('sex', models.CharField(choices=[('MALE', 'male'), ('FEMALE', 'female')], max_length=7)),
                ('weight', models.PositiveIntegerField()),
                ('sheltered_date', models.DateTimeField(verbose_name='Sheltered on ')),
                ('found', models.CharField(max_length=30, verbose_name='Found')),
                ('is_apartment_friendly', models.BooleanField(null=True, verbose_name='Adapts well to apartment living')),
                ('is_family_friendly', models.BooleanField(null=True, verbose_name='Family friendly')),
                ('is_kid_friendly', models.BooleanField(null=True, verbose_name='Kid friendly')),
                ('is_dog_friendly', models.BooleanField(null=True, verbose_name='Dog friendly')),
                ('energy_level', models.CharField(choices=[('LOW', 'Low'), ('MEDIUM', 'Medium'), ('HIGH', 'High')], max_length=10, null=True, verbose_name='Energy level')),
                ('is_good_for_beginners', models.BooleanField(null=True, verbose_name='Good for beginners')),
                ('owner', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.adopter')),
            ],
        ),
    ]
