from django.contrib import admin
from .models import Offer, Preference

admin.site.register(Offer)

admin.site.register(Preference)

