from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

class UserManager(BaseUserManager):
    '''
    User model manager where you can identify users by their email.
    '''

    def create_user(self, email,role, password, **extra_fields):
        '''
        Create and save a User using email,username and password
        '''
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, role=role, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser using email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, ('SHELTER','Shelter'),password, **extra_fields) # I know, this line hurts,
        # but the toughest decisions require the strongest will

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    username = models.CharField(max_length=30, null=True,verbose_name='Your name/Shelter name')

    ROLES = [
        ('ADOPTER','I want to adopt'),
        ('SHELTER','Shelter'),
    ]
    role = models.CharField(max_length=8,choices=ROLES[:2],verbose_name='How will you use the website?',null=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    is_adopter = models.BooleanField(default=False)
    is_shelter = models.BooleanField(default=False)

    def __str__(self):
        return self.email


class Adopter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name='adopters')

    adopter_name = models.CharField(verbose_name='Your name', max_length=30, null=True)
    location = models.CharField(max_length=30, null=True)
    age = models.PositiveIntegerField(null=True)


    is_adopter = models.BooleanField(default=True)




    def __str__(self):
        return self.user.email

class Shelter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    shelter_name = models.CharField(verbose_name='Shelter name',max_length=30,null=True)
    location = models.CharField(max_length=30, null=True)
    website = models.CharField(max_length=30, null=True)
    phone_number = models.CharField(max_length=9, null=True)

    is_shelter = models.BooleanField(default=True)
    def __str__(self):
        return self.user.email

