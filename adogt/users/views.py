from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import CreateView, FormView,UpdateView, ListView, DetailView
from django.contrib.auth import login
from .forms import SignUpForm, PreferenceForm, OfferForm
from .models import User, Adopter, Shelter
from base.models import Preference, Offer
from django.urls import reverse_lazy


class SignUpView(CreateView):
    model = User
    form_class = SignUpForm
    template_name = 'users/signup_form.html'

    # def get_context_data(self, **kwargs):
    #     kwargs['user_type'] = 'buyer'
    #     return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)

        if user.is_adopter:
            return redirect('adopter-create',pk=Adopter.objects.filter(user=user).first().pk)
        if user.is_shelter:
            return redirect('shelter-create',pk=Shelter.objects.filter(user=user).first().pk)



class AdopterCreateView(UpdateView):
    '''
    Creates Adopter profile, or in practice, updates blank fields on already existing model Adopter.
    '''
    model = Adopter
    fields = ['adopter_name','age','location']
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('profile')


class ShelterCreateView(UpdateView):
    '''
    Creates Adopter profile, or in practice, updates blank fields on already existing model Adopter.
    '''
    model = Shelter
    fields = ['shelter_name','location','website','phone_number']
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('profile')

def profile(request) :
    context = {
        'adopter': Adopter.objects.filter(user=request.user).first(),
        'shelter': Shelter.objects.filter(user=request.user).first(),

    }

    return render(request,'base/profile.html',context)


# class AdopterProfileUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
#     model = Adopter
#     fields = '__all__'
#
#     def form_valid(self, form):
#         form.instance.adopter_name = Adopter.objects.filter(user=self.request.user).first().adopter_name
#         form.instance.location = Adopter.objects.filter(user=self.request.user).first().location
#         form.instance.age = Adopter.objects.filter(user=self.request.user).first().age
#         return super().form_valid(form)
#
#     def test_func(self):
#         prof = self.get_object()
#         if self.request.user == prof. Adopter.objects.filter(user=self.request.user).first().adopter_name:
#             return True
#         return False

class AdopterProfileUpdateView(UpdateView):
    model = Adopter
    fields = '__all__'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # def form_valid(self, form):
    #     form.instance.adopter_name = Adopter.objects.filter(user=self.request.user).first().adopter_name
    #     form.instance.location = Adopter.objects.filter(user=self.request.user).first().location
    #     form.instance.age = Adopter.objects.filter(user=self.request.user).first().age
    #     return super().form_valid(form)
    #
    # def test_func(self):
    #     prof = self.get_object()
    #     if self.request.user == prof. Adopter.objects.filter(user=self.request.user).first().adopter_name:
    #         return True
    #     return False




class CreateAdopterPreference(CreateView):
    model = Adopter
    form_class = PreferenceForm
    template_name = 'users/create_adopter_pref.html'

    def form_valid(self, form):
        pref = form.save(commit=False)
        pref.owner = Adopter.objects.filter(user=self.request.user).first()
        pref.save()
        return redirect('profile')


def adopter_pref(request):
    context = {
        'adopter': Adopter.objects.filter(user=request.user).first(),
        'shelter': Shelter.objects.filter(user=request.user).first(),
        'pref':Preference.objects.filter(owner__user__email=request.user).first()

    }
    return render(request,'users/adopter_pref.html',context)



class CreateShelterOffer(CreateView):
    model = Shelter
    form_class = OfferForm
    template_name = 'users/create_shelter_offer.html'

    def form_valid(self, form):
        pref = form.save(commit=False)
        pref.owner = Shelter.objects.filter(user=self.request.user).first()
        pref.save()
        return redirect('profile')

class BrowseOfferView(ListView):
    model = Offer
    template_name = 'base/browse_offers.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'offers'
    #ordering = ['-date_posted']

class OfferDetailView(DetailView):
    model = Offer

