from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms import ModelForm
from base.models import Preference, Offer


from .models import User, Adopter, Shelter

class SignUpForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = [ 'email','password1', 'password2','role']
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        print(user.is_adopter)
        print(user.role)
        if user.role == 'ADOPTER':
            user.is_adopter = True
            user.save()
            print("stworzono ADOPTERA")
            adopter = Adopter.objects.create(user=user)
            return user
        elif user.role == 'SHELTER':
            user.is_shelter = True
            user.save()
            print("stworzono SHELTER")
            shelter = Shelter.objects.create(user=user)
            return user


class PreferenceForm(ModelForm):
    class Meta:
        model = Preference
        exclude = ('owner',)
        #fields = '__all__'

class OfferForm(ModelForm):
    class Meta:
        model = Offer
        exclude = ('owner','sheltered_date')