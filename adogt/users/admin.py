from django.contrib import admin
from .models import User,Adopter, Shelter

admin.site.register(User)
admin.site.register(Adopter)
admin.site.register(Shelter)