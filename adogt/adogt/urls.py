"""adogt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from users.views import (
    SignUpView,AdopterCreateView,profile, ShelterCreateView, CreateAdopterPreference, adopter_pref,
    AdopterProfileUpdateView, CreateShelterOffer,BrowseOfferView, OfferDetailView
)
from django.contrib.auth.views import LoginView, LogoutView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('base.urls')),
    path('signup/',SignUpView.as_view(),name='signup'),
    path('login/',LoginView.as_view(),name='login'),
    path('logout/',LogoutView.as_view(template_name='registration/logout.html'),name='logout'),
    path('create/adopter/<int:pk>',AdopterCreateView.as_view(),name='adopter-create'),
    path('create/shelter/<int:pk>',ShelterCreateView.as_view(),name='shelter-create'),
    path('profile/',profile,name='profile'),
    path('profile/create/pref/',CreateAdopterPreference.as_view(),name='create-pref'),
    path('profile/pref/',adopter_pref,name='adopter-pref'),
    path('profile/create/offer',CreateShelterOffer.as_view(),name='create-offer'),
    path('offers/',BrowseOfferView.as_view(),name='browse-offers'),
    path('offers/<int:pk>',OfferDetailView.as_view(),name='offer-detail'),


    path('profile/update/<int:pk>',AdopterProfileUpdateView.as_view(),name='profile-update'),#!!!!!!!!!!!!

    # path('create/pref/',CreateAdopterPreference.as_view(),name='adopter-pref'),


    ]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)